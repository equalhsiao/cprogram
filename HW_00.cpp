/**author:08 **/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <conio.h>
#include <string.h>
#include <ctype.h>
void welcome() {
	printf("      ,-----.   ,-----.  ,--.   ,--. ,--.   ,--.    \n");
	printf("     '  .-.  ' '  .-.  '  \\  `.'  /   \\  `.'  /   \n");
	printf("     |  | |  | |  | |  |   .'    \\     .'    \\    \n");
	printf("     '  '-'  ' '  '-'  '  /  .'.  \\   /  .'.  \\   \n");
	printf("      `-----'   `-----'  '--'   '--' '--'   '--'    \n");
}
void player1Win() {
	printf("\n");
	printf("          ,--.                                       ,--.               ,--.          \n");
	printf("   ,---.  |  |  ,--,--. ,--. ,--.  ,---.  ,--.--.   /   |   ,--.   ,--. `--' ,--,--,  \n");
	printf("  | .-. | |  | ' ,-.  |  \\  '  /  | .-. : |  .--'   `|  |   |  |.'.|  | ,--. |      \\ \n");
	printf("  | '-' ' |  | \\ '-'  |   \\   '   \\   --. |  |       |  |   |   .'.   | |  | |  ||  | \n");
	printf("  |  |-'  `--'  `--`--' .-'  /     `----' `--'       `--'   '--'   '--' `--' `--''--' \n");
	printf("  `--'                  `---' \n");
}

void player2Win() {
	printf("\n");
	printf("        ,--.                                     ,---.                ,--.           \n");
	printf("   ,---.  |  |  ,--,--. ,--. ,--.  ,---.  ,--.--. '.-.  \\   ,--.   ,--. `--' ,--,--,   \n");
	printf("  | .-. | |  | ' ,-.  |  \\  '  /  | .-. : |  .--'  .-' .'   |  |.'.|  | ,--. |      \\  \n");
	printf("  | '-' ' |  | \\ '-'  |   \\   '   \\   --. |  |    /   '-.   |   .'.   | |  | |  ||  |  \n");
	printf("  |  |-'  `--'  `--`--' .-'  /     `----' `--'    '-----'   '--'   '--' `--' `--''--'  \n");
	printf("  `--'                  `---'                                                          \n");
}
void computerWin() {
	printf("\n");
	printf("                                               ,--.                                 ,--.          \n");
	printf("   ,---.  ,---.  ,--,--,--.  ,---.  ,--.,--. ,-'  '-.  ,---.  ,--.--.   ,--.   ,--. `--' ,--,--,  \n");
	printf("  | .--' | .-. | |        | | .-. | |  ||  | '-.  .-' | .-. : |  .--'   |  |.'.|  | ,--. |      \\ \n");
	printf("  \\ `--. ' '-' ' |  |  |  | | '-' ' '  ''  '   |  |   \\   --. |  |      |   .'.   | |  | |  ||  | \n");
	printf("   `---'  `---'  `--`--`--' |  |-'   `----'    `--'    `----' `--'      '--'   '--' `--' `--''--' \n");
	printf("                            `--'                                                                  \n");
}
void printWiner(char mode,int player) {
	if(mode==49) {
		player==1 ? player1Win() : player2Win();
		return;
	} else if( (mode == 50 && player ==2)) {
		player2Win();
		return ;
	} else if(mode ==51 && player==1) {
		player1Win();
		return;
	}
	computerWin();
}
bool isUnUsed(char c) {
	return c != 88 && c !=79;
}
char chooseMode() {
	char mode = '4';
	while(mode<47||mode>51) {
		printf("\n請選擇遊戲模式:");
		printf("1.單機雙打 ");
		printf("2.人機對弈(電腦先手)");
		printf("3.人機對弈(電腦後手)");
		printf("0.離開\n");
		mode = getche();
	}
	if(mode == 48) {
		printf("\n已選擇離開遊戲");
	} else if(mode == 49) {
		printf("\n已選擇模式:%s","單機雙打\n");
	} else if(mode == 50) {
		printf("\n已選擇模式:%s","人機對弈(電腦先手)\n");
	} else if(mode == 51) {
		printf("\n已選擇模式:%s","人機對弈(電腦後手)\n");
	}
	return mode;
}
void separateTab(int step) {
	for(int i = 0; i< step ; i++) {
		printf(" ");
	}
}

bool replaceTicTacToe(char ticTacToe[][3] , char chooseNumber,int player) {
	for(int i = 0; i<3; i++) {
		for(int j = 0; j< 3; j++) {
			if(ticTacToe[i][j]==chooseNumber) {
				ticTacToe[i][j] = player == 1 ? 'O' : 'X';
//				printf("玩家%d輸入:%s",player,ticTacToe);
				return true;
			}
		}
	}
	return false;
}

//若有則回傳index位置 若無則回傳-1
int* replaceTicTacToeGetIndex(char ticTacToe[][3] , char chooseNumber,int player) {
	int intArray[2] = {0};
	for(int i = 0; i<3; i++) {
		for(int j = 0; j< 3; j++) {
			if(ticTacToe[i][j]==chooseNumber) {
				ticTacToe[i][j] = player == 1 ? 'O' : 'X';
//				printf("玩家%d輸入:%s",player,ticTacToe);
				intArray[0] = i;
				intArray[1] = j;
				return intArray;
			}
		}
	}
	intArray[0] = -1;
	intArray[1] = -1;
	return intArray;
}

void printTicTacToe(char ticTacToe[][3],int step) {
	for(int i = 0 ; i < 3 ; i++) {
		printf("\n");
		separateTab(step);
		printf("一一一一一一\n");
		separateTab(step);
		for(int j = 0 ; j < 3 ; j++) {
			printf("| %c ",ticTacToe[i][j]);
		}
	}
}
void printTicTacToeHistory(char ticTacToeHistory[][3][3],int step,int *index) {
	printf("[%d ,%d]",*index,index[1]);
	int rowIndex = index[0];
	int columnIndex = index[1];
	printf("\n");
	for(int i = 0 ; i < 3; i++) {
		for(int l = 0 ; l <= step; l++) {
			printf("一一一一一一一一  ");
			if(step > l && i == 1) {
				printf("=> ");
			} else {
				printf("   ");
			}
		}
		printf("\n");
		for(int k = 0 ; k<=step; k++) {
			for(int j = 0; j<3; j++) {
				if(k==step&& i == rowIndex && j == columnIndex) {
					printf("| [%c] ",ticTacToeHistory[k][i][j]);
					continue;
				}
				printf("|  %c  ",ticTacToeHistory[k][i][j]);
			}
			printf("   ");
		}
		printf("\n");

	}
	printf("\n");
}

bool check(char ticTacToe[][3],int player,int step) {
	char checkWord = player == 1 ? 79 : 88;
	char checkWord2 = player == 1 ? 88 : 79;
	int leftLine = 0;
	int rightLine = 0;
	int rowLine = 0;
	int columnLine = 0;
	int leftCount = 0;
	int rightCount = 0;
	for(int i = 0 ; i < 3; i++) {
		int rowCount = 0;
		int columnCount = 0;
		for(int j = 0 ; j < 3 ; j++) {
			//row: 	  1[0,0]4[1,0]7[2,0],	2[0,1]5[1,1]8[2,1],	3[0,2]6[1,2]9[2,2]
			if(ticTacToe[j][i] == checkWord) {
				rowCount++;
			}
			if(ticTacToe[j][i] == checkWord2) {
				rowCount--;
			}
			//column: 1[0,0]2[0,1]3[0,2],	4[1,0]5[1,1]6[1,2],	7[2,0]8[2,1]9[2,2]
			if(ticTacToe[i][j] == checkWord) {
				columnCount++;
			}
			if(ticTacToe[i][j] == checkWord2) {
				columnCount--;
			}
		}
		//左斜右 7[0,0]5[1,1]3[2,2]
		if(ticTacToe[i][i] == checkWord) {
			leftCount++;
		}
		if(ticTacToe[i][i] == checkWord2) {
			leftCount--;
		}
		//右斜左 9[0,2]5[1,1]1[2,0]
		for(int j = 2; j >= 0; j--) {
			if(i+j == 2) {
				if( ticTacToe[i][j]==checkWord) {
					rightCount++;
				}
				if( ticTacToe[i][j]==checkWord2) {
					rightCount--;
				}
			}
		}
		if(rowCount == 3) rowLine++;
		if(columnCount == 3) columnLine++;
		if(leftCount == 3) leftLine ++;
		if(rightCount == 3) rightLine++;
	}
	if(rowLine == 1 || columnLine == 1 || rightLine == 1 || leftLine == 1) {
		return true;
	}
	return false;
}
bool isContinueGame(char ticTacToe[][3]) {
	char restart ;
	//78:N , 89:Y
	while(restart != 89 && restart != 78) {
		printf("是否進行下一局?(Y/N)\n");
		restart = toupper(getche());
	}
	return restart==89;
}


//若無回傳0,若有則回傳ASCII碼
int predictWiner(char ticTacToe[][3],int player,int step) {
	//複製
	char copyTicTacToe[][3] = {{ },{ },{ } };
	for(int i = 0 ; i < 3; i++) {
		for(int j = 0 ; j<3; j++ ) {
			copyTicTacToe[i][j] = ticTacToe[i][j];
		}
	}
	//測試是否有連成一線
	bool result = false;
	for(int i = 0 ; i < 3 ; i++) {
		for(int j = 0 ; j<3; j++) {
			if(isUnUsed(copyTicTacToe[i][j])) {
				int temp = copyTicTacToe[i][j];
				replaceTicTacToe(copyTicTacToe,copyTicTacToe[i][j],player);
				result = check(copyTicTacToe,player,step);
				copyTicTacToe[i][j] = temp;
				if(result) {
					return copyTicTacToe[i][j];
				}
			}
		}
	}
	return 0;
}
void keepTicTacToeHistory(char ticTacToeHistory[8][3][3] ,char ticTacToe[3][3],int step) {
	for(int j = 0 ; j < 3; j++) {
		for(int k = 0; k < 3 ; k++) {
			ticTacToeHistory[step][j][k] = ticTacToe[j][k];
		}
	}
}

const char* getPlayerName(char mode,int player) {
	if(mode==49) {
		return "玩家";
	} else if( (mode == 50 && player ==2) || (mode ==51 && player==1)) {
		return "玩家";
	}
	return "電腦";
}

char computerSolution(char ticTacToe[3][3]) {
	char ch;
	if(isUnUsed(ticTacToe[0][0])) {
		ch = '7';
	} else if(isUnUsed(ticTacToe[2][0]) ) {
		ch = '1';
	} else if(isUnUsed(ticTacToe[0][2]) ) {
		//00 11 02
		ch = '9';
	} else if(isUnUsed(ticTacToe[1][1]) ) {
		//00 11 20
		ch = '5';
	} else if(isUnUsed(ticTacToe[0][1]) ) {
		//00 11 02
		ch = '8';
	} else if(isUnUsed(ticTacToe[2][1]) ) {
		//00 11 02
		ch = '2';
	} else {
		char currentChar;
		for(int i = 0; i<3; i++) {
			for(int j = 0 ; j < 3; j++) {
				currentChar = ticTacToe[i][j];
				if(isUnUsed(currentChar)) {
					ch = currentChar;
					break;
				}
			}
			if(isUnUsed(currentChar)) {
				break;
			}
		}
	}
	return ch;
}

bool playGame(char mode) {
	printf("請輸入對應位置數字(1~9) 棄權請輸入0\n");
	char ticTacToeHistory[8][3][3];
	char ticTacToe[][3]= {{'7','8','9' },{'4','5','6' },{'1','2','3' } };
	for(int step=0; step<9; step++) {
		if(step == 0) {
			printTicTacToe(ticTacToe,0);
		}
		int player = (step%2)+1;
		char playerMark = player==1?'O':'X';
		printf("\n\n\n");
		separateTab(step);
		char ch;
		//三步預判
		if(step>5) {

			//兩個玩家剩下兩步下哪都不會贏
			if(step > 6) {
				int test1 = predictWiner(ticTacToe,1,step);
				int test2 = predictWiner(ticTacToe,2,step);
				if(test1==0 && test2 ==0) {
					printf("和局");
					break;
				}
			}
			//剩下一步無法連線
			if(step > 7) {
				int test = predictWiner(ticTacToe,player,step);
				if(test==0 ) {
					printf("和局");
					break;
				}
			}
			//若當前玩家有連線則直接判贏
			int result = predictWiner(ticTacToe,player,step);
			if(result) {
				ch = result;
				int *intArray = replaceTicTacToeGetIndex(ticTacToe,ch,player);
				keepTicTacToeHistory(ticTacToeHistory,ticTacToe,step);
				printTicTacToeHistory(ticTacToeHistory,step,intArray);
				printf("\n%s%c勝利!",getPlayerName(mode,player),playerMark);
				printWiner(mode,player);
				break;
			}
		}
		printf("第%d步 輪到%s:%c\n",step+1,getPlayerName(mode,player),playerMark);
		//人與機

		//電腦先手 || 電腦後手
		if((mode == 50 && player==1) || (player==2 && mode == 51)) {
			//第三步開始檢查 若有連線則選擇
			int result = 0;
			if(step>=3) {
				result = predictWiner(ticTacToe,(mode == 50 ? 2 : 1),step);
			}
			if(result) {
				ch = result;
			} else {
				result = predictWiner(ticTacToe,player,step);
				if(result) {
					ch = result;
				} else {
					ch = computerSolution(ticTacToe);
				}
			}
			printf("電腦輸入:%c",ch);

		}
		//人與人
		else if(mode == 49) {
			printf("請輸入數字:");
			ch = getche();
		} else {
			printf("請輸入數字:");
			ch = getche();
		}

		if(ch==48) {
			printf("\n玩家%c棄權 玩家%c獲勝!\n",playerMark,player+1==1?'O':'X');
			printWiner(mode,player);
			break;
		}
		while(ch<48 || ch>57) {
			printf("\n請輸入0~9的數字\n");
			ch = getche();
		}
		int *intArray = replaceTicTacToeGetIndex(ticTacToe,ch,player);
		if(intArray[0]==-1 ) {
			separateTab(step);
			printf("已重複 請重新輸入數字");
			step--;
		}
		keepTicTacToeHistory(ticTacToeHistory,ticTacToe,step);
		printTicTacToeHistory(ticTacToeHistory,step,intArray);
		if(check(ticTacToe,player,step)) {
			printf("\n%s%c勝利!",getPlayerName(mode,player),playerMark);
			printWiner(mode,player);
			break;
		}
		if(step==8) {
			printf("\n和局\n");
		}
	}
	return isContinueGame(ticTacToe);
}




int main(void) {
	system ("mode con:cols=250 lines=100" );
	welcome();
	char mode = chooseMode();
	if(mode == 48) {
		return 0;
	}
	while(true) {
		while(playGame(mode));
		mode = chooseMode();
		if(mode == 48) {
			break;
		}
	}
	return 0;
}



