#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>


int getTotalDay(int);
int separateTab(int);
int printCalendar(int, int);
int main(void) {
	int year,month;
	time_t t = time(NULL);
	tm tm = *localtime(&t);
	int yearSize = tm.tm_year;
	while(true) {
		printf("請輸入年:");
		scanf("%d",&year);
		fflush(stdin);
		while(year<1900) {
			printf("請輸入大於1900年:");
			scanf("%d",&year);
		};
		printf("請輸入月:");
		scanf("%d",&month);
		fflush(stdin);
		while((month<1 || month > 12)) {
			printf("請輸入1~12間的數字:");
			scanf("%d",&month);
		};
		printCalendar(year,month);
		printf("\n");
	}
	system("PAUSE");
	return 0;
}

int getTotalDay(int year) {
	return (year%400==0) || (year % 4 == 0 ) && (year % 100 != 0) ? 366 : 365;
}

int separateTab(int preMonthremainingDays) {
	for(int i = 0; i<preMonthremainingDays; i++) {
		printf("     ");
	}
}
/*year:要初始化的起始年
  month:要初始化的起始月
  HistoryCalendar:用於儲存紀錄
*/
int printCalendar(int year, int month) {
	//前一個月最後禮拜幾
	int  preMonthEndDayOfWeek = 1;
	//基準1900年1月
	for(int currentYear = 1900; currentYear<=year; currentYear++) {
		int monthOfDay[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
		//2月閏年要+1天
		if((currentYear%400==0) || (currentYear % 4 == 0 ) && (currentYear % 100 != 0)) {
			monthOfDay[1] = 29;
		} else {
			monthOfDay[1] = 28;
		}
		for(int currentMonth = 0; (currentYear < year && currentMonth <=11) || (currentYear == year && currentMonth < month); currentMonth++) {
			if(!(currentYear==year && currentMonth == month-1)) {
				for(int date = 1; date<=monthOfDay[currentMonth]; date++,preMonthEndDayOfWeek++) {
					if(preMonthEndDayOfWeek%7==0 && preMonthEndDayOfWeek != 0 ) {
						preMonthEndDayOfWeek=0;
					}
				}
			} else {
				printf("\n          %d年%d月\n",currentYear,currentMonth+1);
				printf("日   一   二   三   四   五   六\n");
				separateTab(preMonthEndDayOfWeek%7);
				for(int date = 1; date<=monthOfDay[currentMonth]; date++,preMonthEndDayOfWeek++) {
					if(preMonthEndDayOfWeek%7==0 && preMonthEndDayOfWeek != 0 ) {
						preMonthEndDayOfWeek=0;
						printf("\n");
					}
					printf("%2d   ",date);
				}
			}
		}
	}
	return 1;
}

