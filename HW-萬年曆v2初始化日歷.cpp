#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <conio.h>
#define RELEASE
struct HistoryCalendar {
	//這個月有幾天
	int monthOfDay;
	//前月剩下幾天
	int preMonthEndDayOfWeek;
};
HistoryCalendar** initialCalendar(int, int,int,int);
HistoryCalendar ** combineCalendarArray(HistoryCalendar **, HistoryCalendar **,int,int);
void separateTab(int step);
void printCalendar(int , int , int ,HistoryCalendar**);
void debug(HistoryCalendar **,int);
const int defaultStartYear = 1900;
//初始化所儲存的範圍
int startHistoryYear=defaultStartYear,endHistoryYear;
int year,month;
int main(void) {
	time_t t = time(NULL);
	tm tm = *localtime(&t);
	int currentYear = tm.tm_year+defaultStartYear;
	int currentMonth = tm.tm_mon+1;
	endHistoryYear = currentYear;
	//1900~2023
	int size = tm.tm_year+1;
	HistoryCalendar** historyCalendar = initialCalendar(startHistoryYear,endHistoryYear+1,size,1);
	while(true) {
		printf("\n請輸入年:");
		scanf("%d",&year);
		fflush(stdin);
		while(year<defaultStartYear ) {
			printf("請輸入大於1900年:");
			scanf("%d",&year);
		};
		printf("請輸入月:");
		scanf("%d",&month);
		fflush(stdin);
		while(month<1 || month > 12) {
			printf("請輸入1~12間的數字:");
			scanf("%d",&month);
		};
		int preMonthEndDayOfWeek = 0;
		//預設初始化範圍內
		if(year >= defaultStartYear && year <= endHistoryYear) {
			if(year==defaultStartYear && month ==1 ) {
				preMonthEndDayOfWeek = 1;
			} else {
				preMonthEndDayOfWeek++;
				preMonthEndDayOfWeek = month-2 < 0 ?
				                       historyCalendar[(year-defaultStartYear)-1][11].preMonthEndDayOfWeek :
				                       historyCalendar[year-defaultStartYear][month-2].preMonthEndDayOfWeek ;
			}
			printCalendar(year,month,preMonthEndDayOfWeek%7,historyCalendar);
		} else {
			//從上次的結尾開始
			startHistoryYear = endHistoryYear;
			//到指定的年份
			endHistoryYear = year;
			int size2 = endHistoryYear-startHistoryYear;
			//返回一個新的日歷陣列 historyCalendar[size-1][11].preMonthEndDayOfWeek+1) 取得去年最後一筆資料禮拜幾
			HistoryCalendar **historyCalendar2 = initialCalendar(startHistoryYear+1,endHistoryYear+1,size2,(historyCalendar[size-1][11].preMonthEndDayOfWeek)%7);
			//重新合併並得到全新的陣列,因realloc是新的記憶體位置所以要回傳
			historyCalendar = combineCalendarArray(historyCalendar,historyCalendar2,size,size2);
			size  += size2;
			//前一個月的最後禮拜幾 若為一月則取上次變更陣列大小後最後一筆
			if(year==defaultStartYear && month ==1 ) {
				preMonthEndDayOfWeek = 1;
			} else {
				preMonthEndDayOfWeek =month-2 < 0 ?
				                      (historyCalendar[size-2][11].preMonthEndDayOfWeek) :
				                      (historyCalendar[size-1][month-2].preMonthEndDayOfWeek);
			}
			printCalendar(year,month,preMonthEndDayOfWeek%7,historyCalendar);
		}
	}
	free(historyCalendar);
	system("PAUSE");
	return 0;
}
void separateTab(int step) {
	for(int i = 0; i< step ; i++) {
		printf("     ");
	}
}
void debug(HistoryCalendar ** historyCalendar,int size) {
#ifdef DEBUG
	for(int i = 0; i<size; i++) {
		for(int j= 0; j<=11; j++) {
			printf("i=%d,j=%d,historyCalendar[i][j].monthOfDay=%d\n",i,j,historyCalendar[i][j].monthOfDay);
		}
		printf("size=%d",size);
	}
	printf("\nbreak\n");

#endif
}
void printCalendar(int year, int month, int preMonthEndDayOfWeek,HistoryCalendar** historyCalendar) {
	printf("\n          %d年%d月\n",year,month);
	printf("日   一   二   三   四   五   六\n");
	separateTab(preMonthEndDayOfWeek%7);
	for(int date = 1; date<=historyCalendar[ year - 1900 ][ month - 1 ].monthOfDay; date++,preMonthEndDayOfWeek++) {
		if(preMonthEndDayOfWeek%7==0 && preMonthEndDayOfWeek != 0 ) {
			preMonthEndDayOfWeek=0;
			printf("\n");
		}
		printf("%2d   ",date);
	}
	printf("\n");
}

HistoryCalendar ** combineCalendarArray(HistoryCalendar **historyCalendar1 , HistoryCalendar **historyCalendar2 , int size1, int size2) {
	int size = size1 + size2;
	historyCalendar1=(HistoryCalendar**)realloc(historyCalendar1,size * sizeof(HistoryCalendar));
	for(int i = size1; i<size ; i++) {
		historyCalendar1[i]=(HistoryCalendar*)malloc(12 * sizeof(HistoryCalendar));
		for(int j = 0; j<=11; j++) {
			HistoryCalendar calendar;
			calendar.preMonthEndDayOfWeek = historyCalendar2[i-size1][j].preMonthEndDayOfWeek;
			calendar.monthOfDay = historyCalendar2[i-size1][j].monthOfDay;
			historyCalendar1[i][j] = calendar;
		}
	}
	free(historyCalendar2);
	return historyCalendar1;
}


/*year:要初始化的起始年
  month:要初始化的起始月
  calendarSize陣列大小
  calendarBaseOfWeek:日歷基準禮拜幾
*/
HistoryCalendar ** initialCalendar(int startYear,int endYear,int calendarSize,int calendarBaseOfWeek) {
#ifdef DEBUG
	printf("\n初始化中...");
	printf("%d年~%d年 ",startYear,endYear);
	printf("起始基準星期「%d」",calendarBaseOfWeek);
#endif
	//HistoryCalendar:用於儲存紀錄
	HistoryCalendar ** historyCalendar = (HistoryCalendar**)malloc(calendarSize * sizeof(HistoryCalendar[12]));
	//下月從禮拜幾開始 基準1900年1月 禮拜一
	int  preMonthEndDayOfWeek = calendarBaseOfWeek;
	int monthOfDay[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
	for(int currentYear = startYear; currentYear<endYear; currentYear++) {
		historyCalendar[currentYear-startYear]=(HistoryCalendar*)malloc(12 * sizeof(HistoryCalendar));
		//2月閏年要+1天
		if((currentYear%400==0) || (currentYear % 4 == 0 ) && (currentYear % 100 != 0)) {
			monthOfDay[1] = 29;
		} else {
			monthOfDay[1] = 28;
		}
		for(int currentMonth = 0; currentMonth <= 11; currentMonth++) {
			for(int date = 1; date<=monthOfDay[currentMonth]; date++,preMonthEndDayOfWeek++) {
				if(preMonthEndDayOfWeek%7==0 && preMonthEndDayOfWeek != 0 ) {
					preMonthEndDayOfWeek=0;
				}
			}
			HistoryCalendar history;
			history.monthOfDay = monthOfDay[currentMonth];
			history.preMonthEndDayOfWeek = preMonthEndDayOfWeek;
			historyCalendar[currentYear-startYear][currentMonth] = history;
		}
	}
#ifdef DEBUG
	printf("初始化完成\n");
#endif
	return historyCalendar;
}
